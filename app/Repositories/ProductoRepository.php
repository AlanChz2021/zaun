<?php

namespace App\Repositories;

use App\Helpers\ResponseHelper;
use App\Models\Producto;
use Core\Log;
use Exception;
use Illuminate\Database\Eloquent\Collection;

class ProductoRepository
{
    private $model;

    public function __construct()
    {
        $this->model = new Producto();
    }

    public function listar(): Collection
    {
        $datos = [];
        try {
            $datos = $this->model
                ->where('es_borrado',0)
                ->orderBy('nombre', 'DSC')
                ->get();
        } catch (\Exception $e) {
            Log::error(ProductoRepository::class, $e->getMessage());
        }
        return $datos;
    }

    public function guardar($nModelo):ResponseHelper
    {
        $rh = new ResponseHelper();
        try {
            $this->model = $nModelo;
            if (!empty($nModelo->id)) {
                $this->model->exists = true;
            }
            $this->model->save();
            $rh->setResponse(true,'Registro guardado');
        } catch (\Exception $e) {
            Log::error(ProductoRepository::class, $e->getMessage());
        }
        return $rh;
    }

    public function obtener($id): ?Producto
    {
        try {
            $datos = $this->model->find($id);
        } catch (\Exception $e) {
            Log::error(ProductoRepository::class, $e->getMessage());
        }
        return $datos;
    }
}
