<?php

namespace App\Repositories;

use App\Helpers\ResponseHelper;
use App\Models\ClienteModel;
use Core\Log;
use Illuminate\Database\Eloquent\Collection;

class ClientesRepository
{
    private $model;

    public function __construct()
    {
        $this->model = new ClienteModel();
    }

    public function listar():Collection
    {
        $datos=[];

        try{
            $datos=$this->model->where('es_borrado',0)->orderBy('nombre','ASC')->get();
        }catch(\Exception $e){
            Log::error(ClientesRepository::class,$e->getMessage());
        }
        return $datos;
    }

    public function guardar($nModel):ResponseHelper{
        $rh=new ResponseHelper();
        try{
            $this->model=$nModel;

            if(!empty($this->model->id)){
                $this->model->exists=true;
            }

            $this->model->save();
            $rh->setResponse(true,'Se ha guardado correctamente');

        }catch(\Exception $e){
            Log::error(ClientesRepository::class,$e->getMessage());
            $rh->setResponse(false,'Error');
        }
        return $rh;
    }

    public function obtener($id):? ClienteModel{
        try{
            $datos=$this->model->find($id);
        }catch(\Exception $e){
            Log::error(ClientesRepository::class,$e->getMessage());
        }
        return $datos;
    }
}