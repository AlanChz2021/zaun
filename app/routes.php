<?php

/* Controllers */
$router->group(['before' => 'auth'], function ($router) {
    $router->controller('/root', 'App\\Controllers\\RootController');
    $router->controller('/panel', 'App\\Controllers\\PanelController');
    $router->controller('/clientes', 'App\\Controllers\\ClientesController');
});

$router->controller('/auth', 'App\\Controllers\\AuthController');
$router->controller('/home', 'App\\Controllers\\HomeController');

$router->get('/', function () {

    /*if(!\Core\Auth::isLoggedIn()){
        \App\Helpers\UrlHelper::redirect('auth');
    } else {
        \App\Helpers\UrlHelper::redirect('home');
    }*/

    //Si quieres tener una pagina publica
    \App\Helpers\UrlHelper::redirect('home');
});

$router->get('/welcome', function () {
    return 'Welcome page';
}, ['before' => 'auth']);

$router->get('/test', function () {
    return 'Plantilla desarrollada por SuZuMa';
}, ['before' => 'auth']);
