<?php


namespace App\Validations;

use Respect\Validation\Validator as v;
use App\Helpers\ResponseHelper;
use App\Models\Libro;

class LibroValidation
{
    public static function Validar(array $model)
    {
        try {
            $v = v::key('txttitulo', v::stringType()->notEmpty())
                ->key('txtdescripcion', v::stringType()->notEmpty())
                ->key('txtprecio', v::stringType()->notEmpty());

            $v->assert($model);
        } catch (\Exception $e) {
            $rh = new ResponseHelper();
            $rh->setResponse(false, null);
            $rh->validations = $e->findMessages([
                'txttitulo' => 'Campo es requerido',
                'txtdescripcion' => 'Campo es requerido',
                'txtprecio' => 'Precio > 50'
            ]);

            exit(json_encode($rh));
        }
    }
}
