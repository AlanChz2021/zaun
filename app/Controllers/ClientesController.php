<?php
namespace App\Controllers;

use App\Helpers\UrlHelper;
use App\Models\ClienteModel;
use App\Repositories\ClientesRepository;
use Core\Controller;

class ClientesController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getindex(){
        $objRep=new ClientesRepository();
        return $this->render('clientes/index.twig',[
            'title'=>'Modulo de clientes',
            'clientes'=>$objRep->listar()
        ]);
    }

    public function getformulariog($id=null)
    {
        $datos=null;
        if($id!=null){
            $objRep=new ClientesRepository();
            $datos=$objRep->obtener($id);
        }
        return $this->render('clientes/formulario.twig',[
            'title'=>'Ficha del cliente',
            'datos'=>$datos
        ]);
    }

    public function postformulario(){

        $model=new ClienteModel();
        foreach($_POST as $key => $value){
            $model->$key=$value;
        }

        $objRep=new ClientesRepository();
        $rh=$objRep->guardar($model);

        if($rh->response){
            $rh->href='clientes';
        }
        print_r(json_encode($rh));
    }

    public function getborrar($id=null){
        if($id==null){
            UrlHelper::redirect('clientes');
        }

        $objRep= new ClientesRepository();
        $datos=$objRep->obtener($id);

        return $this->render('clientes/borrar.twig',[
            'title'=> 'Borrando cliente',
            'datos'=>$datos
        ]);
    }

    public function postborrar(){
        $id=$_POST['id'];
        $objRep=new ClientesRepository();
        $model=$objRep->obtener($id);
        $model->es_borrado=1;

        $rh=$objRep->guardar($model);
        if($rh->response){
            $rh->href='clientes';
        }

        print_r(json_encode($rh));
    }
}