<?php

namespace App\Controllers;

use Core\{Auth, Controller, Log};

class HomeController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getIndex()
    {
        return $this->render('home/index.twig', [
            'title' => 'Inicio',
            'menu' => false
        ]);
    }

    public function getSignin()
    {
        Auth::signIn([
            'id' => 3,
            'name' => 'Noe',
        ]);
    }

    public function getUser()
    {
        var_dump(Auth::getCurrentUser());
    }

    public function getIsloggedin()
    {
        return var_dump(Auth::isLoggedIn());
    }
}
