<?php

namespace App\Controllers;

use App\Helpers\UrlHelper;
use App\Models\Producto;
use App\Repositories\ProductoRepository;
use Core\Controller;

class PanelController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getindex()
    {
        print_r('Hola Usuario Logueado');
    }

    public function getproductos()
    {
        $productoRepo = new ProductoRepository();
        $datos = $productoRepo->listar();

        return $this->render('panel/index.twig', [
            'title' => 'Listado de Productos',
            'datos' => $datos,
            'menu' => true
        ]);
    }

    public function getaddproducto($id = null)
    {
        $dato = null;
        if ($id != null) {
            $prodRepo = new ProductoRepository();
            $dato = $prodRepo->obtener($id);
        }
        return $this->render('panel/addproducto.twig', [
            'title' => 'Add/Update Producto',
            'dato' => $dato
        ]);
    }

    public function postsaveproducto()
    {
        $model = new Producto();
        /*
        $model->nombre = $_POST['nombre'];
        $model->descripcion = $_POST['descripcion'];
        $model->precio = $_POST['precio'];
        $model->id=$_POST['id'];
        */

        foreach ($_POST as $key => $value) {
            $model->$key = $value;
        }

        $prodRepo = new ProductoRepository();

        $rh = $prodRepo->guardar($model);
        if ($rh->response) {
            $rh->href = 'panel/productos';
        }
        print_r(json_encode($rh));
    }

    public function getborrar($id = null)
    {
        if ($id == null) {
            UrlHelper::redirect('panel/productos');
        }
        $objRepo = new ProductoRepository();
        $datos = $objRepo->obtener($id);

        return $this->render('panel/borrar.twig', [
            'title' => 'Borrando Productos',
            'datos' => $datos
        ]);
    }

    public function postborrar()
    {
        $id = $_POST['id'];
        $objRep = new ProductoRepository();
        $modelo = $objRep->obtener($id);
        $modelo->es_borrado = 1;

        $rh = $objRep->guardar($modelo);
        if ($rh->response) {
            $rh->href = 'panel/productos';
        }
        print_r(json_encode($rh));
    }
}
