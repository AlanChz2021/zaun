<?php

namespace App\Controllers;

use App\Repositories\ClienteRepository;
use Core\Controller;

class RootController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getindex()
    {
        $objRepo = new ClienteRepository();
        $datos = $objRepo->listar();
        return $this->render('root/index.twig', [
            'autor' => 'Alan Chavez',
            'datos' => $datos
        ]);
    }
}
